import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Section3LoginComponent } from './section3-login.component';
describe('Section3LoginComponent', () => {
  let component: Section3LoginComponent;
  let fixture: ComponentFixture<Section3LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Section3LoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Section3LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  
});
