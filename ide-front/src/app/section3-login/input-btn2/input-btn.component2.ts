import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { AssociationService } from "src/app/association.service";

@Component({
  selector: "app-input-btn2",
  templateUrl: "./input-btn.component2.html",
  styleUrls: ["./input-btn.component2.scss"]
})
export class InputBtnComponent2 implements OnInit {
  association = {
    email: "",
    password: ""
  };
  connectionForm: FormGroup;

  constructor(
    private router: Router,
    private associationService: AssociationService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.createFormForAssoCo();
  }

  createFormForAssoCo() {
    this.connectionForm = this.fb.group({
      email: "",
      password: ""
    });
  }

  connection() {
    if (this.connectionForm.valid) {
      console.log("ASSO est pas mal", this.connectionForm);
      console.log(this.connectionForm.value);
      if (
        this.association.email === "admin@admin.fr" &&
        this.association.password === "admin"
      ) {
        window.location.href = "http://localhost:4201/";
      } else {
        this.associationService
          .connectAsso(this.connectionForm.value)
          .subscribe(result => {
            console.log("bonjou", result, "asso créer");
            if (result["success"] === true) {
              this.router.navigate(["/questions"]);
            } else {
              alert(result["message"]);
            }
          });
      }
    }
  }
}
