import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputBtnComponent2 } from './input-btn.component2';

describe('InputBtnComponent2', () => {
  let component: InputBtnComponent2;
  let fixture: ComponentFixture<InputBtnComponent2>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputBtnComponent2 ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputBtnComponent2);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
