import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-section1-home',
  templateUrl: './section1-home.component.html',
  styleUrls: ['./section1-home.component.scss']
})
export class Section1HomeComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    $('.btn-s1').on('click', function() {
      /**
       * Smooth scrolling to a specific element
       **/
      function goSectionII(target: JQuery<HTMLElement>) {
        if (target.length) {
          $('html, body')
            .stop()
            .animate({ scrollTop: target.offset().top }, 900);
        }
      }
      // exemple
      goSectionII($('#smooth'));
    });
  }
}
