import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Section1HomeComponent } from './section1-home/section1-home.component';
import { Section2InscriptionComponent } from './section2-inscription/section2-inscription.component';
import { InputBtnComponent } from './section2-inscription/input-btn/input-btn.component';
import { InputBtnComponent2 } from './section3-login/input-btn2/input-btn.component2';
import { Section3LoginComponent } from './section3-login/section3-login.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatSliderModule } from '@angular/material/slider';

// Text field for section 2 && 3
import { MatFormFieldModule } from '@angular/material/form-field';

// Import Input
import { MatInputModule, MatToolbarModule } from '@angular/material';

// Import Icon from Material
import { MatIconModule } from '@angular/material/icon';

// Button wow
import { MatButtonModule } from '@angular/material/button';
import { Section4QuestionsComponent } from './section4-questions/section4-questions.component';
import { NavComponent } from './section4-questions/nav/nav.component';
// Grid
import { MatGridListModule } from '@angular/material/grid-list';



@NgModule({
	declarations: [
		AppComponent,
		Section1HomeComponent,
		Section2InscriptionComponent,
		InputBtnComponent,
		Section3LoginComponent,
		HomeComponent,
		Section4QuestionsComponent,
		NavComponent,
		
		
		
		InputBtnComponent2
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		MatSliderModule,
		MatFormFieldModule,
		MatIconModule,
		MatInputModule,
		MatButtonModule,
		HttpClientModule,
		ReactiveFormsModule,
		FormsModule,
		MatToolbarModule,
		MatGridListModule
	],
	providers: [],
	bootstrap: [ AppComponent ]
})
export class AppModule {}
