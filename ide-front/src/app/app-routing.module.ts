import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { Section4QuestionsComponent } from './section4-questions/section4-questions.component';


const routes: Routes = [{ path: "", component: HomeComponent }, { path: "questions", component: Section4QuestionsComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
