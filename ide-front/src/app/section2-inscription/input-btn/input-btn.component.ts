import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, NgForm } from "@angular/forms";
import { association } from "../../model/association";
import { Observable } from "rxjs";
import { AssociationService } from "../../association.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-input-btn",
  templateUrl: "./input-btn.component.html",
  styleUrls: ["./input-btn.component.scss"]
})
export class InputBtnComponent implements OnInit {
  association = {
    name: "",
    email: "",
    password: ""
  };
  creationForm: FormGroup;

  constructor(
    private router: Router,
    private associationService: AssociationService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.createFormForAsso();
  }

  createFormForAsso() {
    this.creationForm = this.fb.group({
      name: "",
      email: "",
      password: ""
    });
  }

  CreateAssoNew() {
    if (this.creationForm.valid) {
      //   console.log("ASSO est pas mal", this.creationForm);
      this.associationService
        .createAsso(this.creationForm.value)
        .subscribe(data => {
          console.log(data);
          if (data["success"]) {
            alert(
              "Votre demande a bien été envoyée, vous serez contacté par mail!"
            );
          } else {
            console.log(data);
            alert(data["message"]);
          }
          //   console.log(data, "asso créer");
        });
    }
  }
}
