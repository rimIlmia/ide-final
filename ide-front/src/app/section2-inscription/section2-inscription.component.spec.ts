import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Section2InscriptionComponent } from './section2-inscription.component';

describe('Section2InscriptionComponent', () => {
  let component: Section2InscriptionComponent;
  let fixture: ComponentFixture<Section2InscriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Section2InscriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Section2InscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
