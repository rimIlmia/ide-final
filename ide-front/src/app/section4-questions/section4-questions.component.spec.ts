import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Section4QuestionsComponent } from './section4-questions.component';

describe('Section4QuestionsComponent', () => {
  let component: Section4QuestionsComponent;
  let fixture: ComponentFixture<Section4QuestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Section4QuestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Section4QuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
