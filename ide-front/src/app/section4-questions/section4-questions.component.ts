import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-section4-questions",
  templateUrl: "./section4-questions.component.html",
  styleUrls: ["./section4-questions.component.scss"]
})
export class Section4QuestionsComponent implements OnInit {
  valide: boolean;
  visible = true; // visibilité buttons Oui et Non
  nbrpersonne: string = "1"; //input nombre de personnes
  private apiUrl = "http://localhost:5000/regr";
  question = "";
  ques = "";
  name: string = "";
  pers = false; // visibilité input & button valider pour question nbre personnes

  constructor(private http: HttpClient) {
    this.getQuestion();
  }
  getQuestion() {
    console.log("Wesh");
    let res = this.http.get(this.apiUrl).subscribe(data => {
      this.ques = data["doc"];
    });
  }
  reponse(rep, personne) {
    this.visible = true; //oui et non
    let formData: FormData = new FormData();
    //Le cas où on a un nombre de personnes (La reponse differente de Oui ou non)
    if (rep === null) {
      formData.append("n_pers", this.nbrpersonne);
      this.visible = true;
      this.pers = false;
    }
    //Le cas de reponse Oui ou non
    if (rep) {
      formData.append("choice", rep);
    }

    this.http.post(this.apiUrl, formData).subscribe(data => {
      document.getElementById("question").innerHTML = data["doc"];
      this.ques = data["doc"];
      if (data["personnes"]) {
        this.pers = true;
        this.visible = false;
      }
      if (data["end_of_tree"]) {
        this.visible = false;
      }
    });
  }

  ngOnInit() {
    
  }
}
