import { Injectable } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { association } from './model/association';

@Injectable({
	providedIn: 'root'
})
export class AssociationService {
	association = {
		name: '',
		email: ''
	};
	creationForm: FormGroup;
	APIadress = 'http://localhost:3000/';
	data: any;

	constructor(private httpClient: HttpClient) {}

	createAsso(associationPost: association) {
		console.log('create user');
		console.log(associationPost);
		return this.httpClient.post<association>(`${this.APIadress}association`, associationPost);
	}

	connectAsso(associationPost: association) {
		console.log("Connect User");
		console.log(associationPost);
		return this.httpClient.post<association>(`${this.APIadress}coAssociation`, associationPost);
	}
}
