from flask import Flask, request, render_template
from pymongo import MongoClient
from flask_cors import CORS

db  = MongoClient().ide
app = Flask(__name__)
CORS(app)

zone = 'A'
n_pers = ''
current_doc = list(db.regroupement_familial.find({"_id" : "RF-GLOB-0-0"}))[0]
print (current_doc)
@app.route('/regr', methods=['POST', 'GET'])
def regroupement_familial():
    
    global current_doc, n_pers 
    print (current_doc) 
    if request.method == 'POST':
        if current_doc['_id'] == 'RF-GLOB-0':
            user_resp = 'OK'
            n_pers    = int(request.form.get('n_pers'))
        else:
            user_resp = request.form.get('choice')

        try:
            current_doc = list(db.regroupement_familial.find({"_id" : current_doc[user_resp]}))[0]
        except IndexError : #  { doc: current_doc['Titre]}
            return { "doc": current_doc[user_resp], "end_of_tree": True}
            #return render_template('regroup_fam.html', doc =  current_doc[user_resp], end_of_tree = True)

        if current_doc['_id'] == 'RF-GLOB-2':

            return {"doc":current_doc['Titre']+' '+str(calculate_salary(n_pers))}
            #return render_template('regroup_fam.html', doc = current_doc['Titre']+' '+str(calculate_salary(n_pers)))

        elif current_doc['_id'] == 'RF-GLOB-1':
            return{"doc":current_doc['Titre']+ " " +str(calculate_meters(n_pers, zone))}
            #return render_template('regroup_fam.html', doc = current_doc['Titre']+ " " +str(calculate_meters(n_pers, zone)))
        elif current_doc['_id'] == 'RF-GLOB-0':
            return {"doc":current_doc['Titre'], "personnes":True} # have to change
            #return render_template('regroup_fam.html', doc = current_doc['Titre'], personnes=True) # have to change

        else:
            return {"doc":current_doc['Titre']}
    else:
        print (current_doc)
        current_doc = list(db.regroupement_familial.find({"_id" : "RF-GLOB-0-0"}))[0]

        return { "doc": current_doc['Titre']}
        #return render_template('regroup_fam.html', doc = current_doc['Titre'])


def calculate_salary(n_pers):
    if n_pers in [1,2] : return "1 521,22 e Brut" 
    elif n_pers in [3,4] : return "1 673.34 e Brut"
    else: return "1 825.46 e Brut"

def calculate_meters(n_pers, zone):
    if zone == 'A':
        return 22 if n_pers == 1 else 74 if n_pers in range(2,8) else 177
    if zone == 'B':
        return 24 if n_pers == 1 else 76 if n_pers in range(2,8) else 179
    if zone == 'C':
        return 28 if n_pers == 1 else 78 if n_pers in range(2,8) else 183







if __name__ == '__main__':
    app.run(debug=True)






# pipeline_glob_1 = [{'$match': {user_zone: {'$exists': True}}},
#         {'$project': {'result': {'$filter': {
#         'input': '${}.person'.format(user_zone),
#         'as': 'k',
#         'cond': {'$in': [int(n_pers), '$$k.count']}}}}},
#         {'$project': {'m2': '$result.m2', '_id': 0}}]
# meters = list(db.regroupement_familial.aggregate(pipeline_glob_1))[0]['m2'][0]






# regul_db = MongoClient().regul

# memory = list(regul_db.coll.find({'_id' : 'START'}))[0]

# @app.route('/', methods=['GET', 'POST'])
# def regul():
#     global memory
#     doc = memory
#     if request.method == 'POST':
#         resp = request.form.get('oui_ou_non')
#         suivant = list(regul_db.coll.find({'_id' : (doc[resp])}))[0]
#         memory = suivant
#         print(memory)
#         return render_template("regul.html", doc=suivant['title'])
#     return render_template("regul.html", doc=doc['title'])
