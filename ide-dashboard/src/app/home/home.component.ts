import { Component, OnInit, Input } from '@angular/core';
import { GetAssoService } from '../get-asso.service';
import { Router } from '@angular/router';
import { association } from '../model/association';
import { Observable } from 'rxjs';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: [ './home.component.scss' ]
})
export class HomeComponent implements OnInit {
	@Input() assoData;

	assoGetList$: Observable<association[]>;
	public assoList: any;

	constructor(private getAssoService: GetAssoService, private router: Router) {
		this.getAssoService.getData().then((data) => {
			console.log(data);
			this.assoList = data;
		});
	}

	ngOnInit() {
		this.assoGetList$ = this.getAssoService.getAllAsso();
	}
}
