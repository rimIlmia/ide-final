import { Component, OnInit, Input } from "@angular/core";
import { GetAssoService } from "../get-asso.service";
import { Router } from "@angular/router";
import { association } from "../model/association";
import { Observable } from "rxjs";

@Component({
  selector: "app-asso-card",
  templateUrl: "./asso-card.component.html",
  styleUrls: ["./asso-card.component.scss"]
})
export class AssoCardComponent implements OnInit {
  @Input() assoData;

  data: any;

  assoGetList$: Observable<association[]>;

  public assoList: any;

  constructor(private getAssoService: GetAssoService, private router: Router) {
    this.getAssoService.getData().then(data => {
      console.log(data);
      this.assoList = data;
    });

    // this.getAssoService
    //   .deleteAsso()
    //   .toPromise()
    //   .then(data => {
    //     console.log(data);
    //     this.assoList = data;
    //   });
  }

  ngOnInit() {
    this.assoGetList$ = this.getAssoService.getAllAsso();
  }

  deleteAsso(id) {
    this.getAssoService.deleteAsso(id).subscribe(res => {


      console.log(res);
      if(res["success"])
      {
      location.reload(); 

      }
    });
  }
  activer( email:string) {
    this.getAssoService.activateAccount(email).subscribe(res => {
   if(res["success"])
      {
      location.reload(); 

      }
      

  
    });
  }
}
