const express = require("express");
const app = express();
const port = process.env.PORT || 3000;
const uuidv1 = require("uuid/v1");
const bodyParser = require("body-parser");
const fs = require("fs");
const cors = require("cors");
const bcrypt = require("bcrypt");
const nodemailer = require("nodemailer");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.json());
const filename = "association.json";
app.use(cors());
/****************Connexion-login******/
app.post("/coAssociation", async (req, res) => {
  try {
    console.log(req.body);

    fs.exists(filename, exists => {
      if (exists) {
        fs.readFile(filename, (err, data) => {
          console.log(data);
          //if fs exists read my file and call callback function
          let users = JSON.parse(data);
          console.log(users);

          let isThere = "";
          users.forEach(element => {
            if (element.email === req.body.email) {
              if (element.active === false) {
                res.send({
                  success: false,
                  message: "Compte en attente d'activation"
                });
              } else if (
                bcrypt.compare(
                  req.body.password,
                  element.password,
                  (erreur, response) => {
                    if (response) {
                      console.log("******************* password bon");
                      res.send({ success: true });
                    } else {
                      console.log("******************* password  pas bon");

                      res.send({
                        success: false,
                        message: "Password incorrect"
                      });
                    }
                    if (erreur) {
                      console.log("erreur bcrypt");
                    }
                  }
                )
              )
                console.log(element);
              isThere = "yes";
            }
          });
          if (isThere !== "yes") {
            res.send({
              success: false,
              message: "Vous n'avez pas de compte"
            });
          }
        });
      }
    });
  } catch {
    res.status(500).send();
  }
});
/************Create Account-Inscription******/
app.post("/association", async (req, res) => {
  // try {
  const salt = await bcrypt.genSalt();
  const hashedPassword = await bcrypt.hash(req.body.password, salt);
  let users = [];
  let isThere = false;
  // try {
  fs.exists(filename, exists => {
    if (exists) {
      fs.readFile(filename, (err, data) => {
        //if fs exists read my file and call callback function
        users = JSON.parse(data);
        for (let elem in users) {
          console.log(elem);
          if (!isThere && users[elem].email === req.body.email) {
            isThere = true;
            res
              .status(401)
              .send({ success: false, msg: "Cet email existe déjà" });
            break;
          }
        }
        // users.forEach(elem => {
        //   if (elem.email === req.body.email) {
        //     res
        //       .status(500)
        //       .send({ success: false, msg: "Cet email existe déjà" });
        //     break;
        //     // return;
        //     // res.end();
        //   }
        // });

        let user = {
          name: req.body.name,
          email: req.body.email,
          password: hashedPassword,
          active: false
        };
        user.id = uuidv1(); // add id to json

        users.push(user);
        // console.log(users);
        let users_string = JSON.stringify(users, null, 2);

        fs.writeFile(filename, users_string, err => {
          if (err) {
            console.log("LALALALALALALALALALALA");
            throw err;
          } else if (!isThere) {
            res.status(200).send({ success: true });
            res.end();
          }
        });
      });
    } else {
      users = [];
      let user = {
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword
      };
      user.id = uuidv1(); // add id to json
      users.push(user);
      // console.log(users);
      let users_string = JSON.stringify(users, null, 2);

      fs.writeFile(filename, users_string, err => {
        if (err) {
          console.log("ICICICICICIICICICICICICICI BOOOM");
          throw err;
        } else {
          if (!isThere) {
            res.status(200).send({ success: true });
            res.end();
          }
        }
      });
    }
  });
  // }
  //users.push(user);
  // }
  //  catch (e) {
  //   console.log("ERROROROROROROROR");
  //   console.log(e);
  //   res.status(500).send({ msg: "ERROROROROROROROR" });
  // }
});

/** */
app.get("/dashboard", (req, res) => {
  fs.exists(filename, exists => {
    if (exists) {
      fs.readFile(filename, (err, data) => {
        res.write(data);
        res.send();
      });
    } else {
      let users = [];
    }
  });
});

/********** Activate account ************/

app.put("/table/:email", (req, res) => {
  let tabUsers = [];
  let email = req.params.email;
  let transporter = nodemailer.createTransport({
    host: "smtp-mail.outlook.com", // hostname
    secureConnection: false, // TLS requires secureConnection to be false
    port: 587, // port for secure SMTP
    service: "outlook",
    tls: {
      ciphers: "SSLv3"
    },
    debug: false,
    logger: true,
    auth: {
      user: "Aiuquet@outlook.fr",
      pass: "5726Simplon92"
    }
  });
  transporter.sendMail(
    {
      from: "Aiuquet@outlook.fr",
      to: email,
      subject: "DEMANDE ACCEPTEE",
      text:
        "Votre demande a bien été traitée et vous pouvez dès maintenant acceder à votre compte :)"
    },
    function(error, info) {
      if (error) {
      } else {
        console.log("************Email sent: " + info.response);
        let tabUsers = [];
        fs.readFile(filename, (err, data) => {
          tabUsers = JSON.parse(data);
          console.log(tabUsers);
          for (var i = 0; i < tabUsers.length; i++) {
            if (email === tabUsers[i].email) {
              tabUsers[i].active = true;
              break;
            }
          }
          let users_string = JSON.stringify(tabUsers, null, 2);
          fs.writeFile(filename, users_string, err => {
            if (err) throw err;
            else res.status(200).send({ success: true, message: "Activé" });
          });
        });
      }
    }
  );
});
/********Delete account*** */

app.delete("/table/:id", (req, res) => {
  let tabUsers = [];
  let id = req.params.id;
  fs.readFile(filename, (err, data) => {
    tabUsers = JSON.parse(data);
    console.log(tabUsers);
    for (var i = 0; i < tabUsers.length; i++) {
      if (id === tabUsers[i].id) {
        tabUsers.splice(i, 1);
        console.log("delete");
        break;
      }
    }
    let users_string = JSON.stringify(tabUsers, null, 2);
    fs.writeFile(filename, users_string, err => {
      console.log("agaga");
      if (err) throw err;
    });
    res.status(200).send({ success: true, message: "supprimer ok" });
  });
});

app.listen(port, () => {
  console.log(`api connecter sur le port ${port}`);
});
